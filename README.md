# About

This is built on the work in the sandbox: https://www.drupal.org/sandbox/onkeltem/2828817
and
https://github.com/thinktandem/migrate_booster

On Drupal 7 we could disable hooks while running migrations:

    https://www.drupal.org/node/2136601

This module adds a similar feature. You can disable hooks
in settings.php or by editing configuration object `migrate_boost.settings`.

There are two ways to disable hooks:

1) Disable specific hooks and modules:

```
$config['migrate_boost.settings']['hooks'] = [
  # Entity insert
  'entity_insert' => [
    'workbench_moderation',
    'pathauto',
    'xmlsitemap',
  ],
  # Entity presave
  'entity_presave' => [
    'xmlsitemap',
  ],
  # Entity predelete
  'entity_predelete' => [
    'flag',
  ],
];
```

2) Disable all hooks of specific modules:

```
$config['migrate_boost.settings']['modules'] = [
  'workbench_moderation',
  'pathauto',
  'xmlsitemap',
];
```
