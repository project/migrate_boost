<?php

namespace Drupal\migrate_boost;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Provides a MyModuleSubscriber.
 */
class HooksEnablerSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['ensureHooksEnabled', 20];
    return $events;
  }

  /**
   * Triggers on 'kernel.request' event.
   *
   * Not when Drush or Drupal console command runs.
   */
  public function ensureHooksEnabled() {
    MigrateBoost::bootDrupal();
  }

}
