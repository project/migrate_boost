<?php

namespace Drupal\migrate_boost\Drush\Commands;

use Consolidation\AnnotatedCommand\AnnotationData;
use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Utility\Token;
use Drupal\migrate_boost\MigrateBoost;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 */
final class MigrateBoostCommands extends DrushCommands {

  /**
   * @hook pre-command migrate:import
   */
  public function preCommandMigImport(CommandData $commandData)
  {
    // Do something before some:command
    $this->boosterEnable();
  }

  /**
   * @hook pre-command migrate:rollback
   */
  public function preCommandMigRollback(CommandData $commandData)
  {
    // Do something before some:command
    $this->boosterEnable();
  }

  /**
   * Resets migrate booster and implementation cache.
   *
   * @command migrate:booster:reset
   *
   * @validate-module-enabled migrate_boost
   * @aliases mbr,migrate-booster-reset
   */
  public function boosterReset() {
    MigrateBoost::reset();
    $this->logger()->notice(dt('Migrate Boost reset'));
  }

  /**
   * Enables migrate booster and implementation cache.
   *
   * @command migrate:booster:enable
   *
   * @validate-module-enabled migrate_boost
   * @aliases mbe,migrate-booster-enable
   */
  public function boosterEnable() {
    MigrateBoost::enable();
    $this->logger()->notice(dt('Migrate Boost enabled'));
  }

}
